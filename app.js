var express = require('express')
	, app = express()
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
 	, server = require('http').createServer(app)
  , io = require('socket.io').listen(server);


if('MONGOHQ_URL' in process.env){
  mongoose.connect(process.env['MONGOHQ_URL']);
}else{
  mongoose.connect("mongodb://localhost/chat-client");
}


app.configure(function () {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.static(path.join(__dirname, 'public')));
});


var port = process.env.PORT || 3000;
server.listen(port);


var Msg = mongoose.model('Msg', { msg: String, usr: String });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () { console.log('db connected'); });


io.configure(function () { 
  io.set("transports", ["xhr-polling"]); 
  io.set("polling duration", 20); 
});

io.sockets.on('connection', function (socket) {
  socket.on('chat', function (data) {
    console.log(data);
    socket.broadcast.emit('chat',{ msg: data.msg, usr: data.usr });
    saveMessage(data.usr,data.msg);
  });

  socket.on('delete', function (data){
    Msg.findById( data.mid, function (err,doc) {
      var id = doc._id;
      doc.remove(function(err){
        if(err){
          console.log(err);
        }else{
          socket.emit('delete', {id: id});
        }
      });
      doc.save();
    });
  });
});

function saveMessage(usr,msg){
  var message = new Msg({ msg: msg, usr: usr });
  message.save(function (err) {});
}


app.get('/', function (req, res) {
  Msg.find(function(err, messages){
    console.log(messages);
    res.render('index', { messages: messages });
  });
});
