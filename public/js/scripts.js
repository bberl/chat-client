$(document).ready(function(){
	var socket = io.connect();

	socket.on('connect', function () {
	  message('System', 'Connected');
	});
	socket.on('error', function (e) {
	  message('System', e ? e : 'A unknown error occurred');
	});
	socket.on('chat', function (data) {
	  message(data.usr, data.msg);
	});
	socket.on('delete', function (data) {
		$('.' + data.id).fadeOut();
	});

	$("input.text").keypress(function(e){
		if(e.which == 13) {
			msg = $('input.text').val();
			usr = $("input.name").attr('value');
			$('input.text').val('');
			socket.emit('chat',{ msg: msg, usr: usr });
			message(usr, msg);
		}
		//return false;
	});

	function message(from, msg) {
	  $('#messages').append($('<p>').append($('<b>').text(from), " " + msg));
	}

	$("#msg-input").hide();
	$("input.name").keypress(function(e){
		if(e.which == 13) {
			$(this).attr("type","hidden");
			$("#msg-input").show();
		}
	});

	$("a.delete").on('click', function(){
		var id = $(this).parent().attr("class");
		socket.emit('delete', {mid:id});
		return false;
	});
});